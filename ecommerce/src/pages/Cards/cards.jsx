import React, { Component } from 'react'
import './cards.css'

import { connect } from 'react-redux'
import {addBasket} from '../../actions/addAction'
import { bindActionCreators } from 'redux'

class Card extends Component {

    render() {
    return (
        <div className="Home">
            <div className="Card">
                <div className="Conteudo">
                    <img className="Img" src={this.props.produtos.image} alt={this.props.produtos.name} />
                </div>
                <div className="Produto"> {this.props.produtos.name} </div>
                <div className="Infos">
                    <p>R$ {this.props.produtos.price}</p>
                </div>
                <button onClick = {() => this.props.addBasket(this.props.produtos)} className="BtComprar">Adicionar ao carrinho</button>
                
            </div>
        </div>
    )
}
}

const mapDispatchToProps = dispatch => bindActionCreators({ addBasket }, dispatch)


export default connect(null, mapDispatchToProps)(Card)