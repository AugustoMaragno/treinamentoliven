import React from 'react'
import './cart.css'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {removeItem } from '../../actions/removeItem'
import {fimPedido} from '../../actions/fimPedido'

function Cart({ basketProps, removeItem, fimPedido }) {
    let productsInCart = []
 console.log(basketProps)
    productsInCart = basketProps.prodAlod.map((produto, index) => {

        return (
            <div key={index} className="cart" >
                <div className="product">
                    <button onClick={() => removeItem(produto)} className="close">X</button>
                    <div className="ImgProd">
                    <img src={produto.image} alt={produto.name} />
                    </div>
                    <span className="nomeProduto">{produto.name}</span>
                    <div className="preço">R$ {produto.price}</div>
                </div>
                
            </div >
        )

    })

    return (
        <div className="Carrinho">
        <div className="container-products" >
            <div className="product-header">
                <h5 className="product-title">ITEM</h5>
                <h5 className="price sm-hide">PREÇO</h5>
            </div>
            <div className="products">
                {productsInCart}
            </div>
            <div className="basketTotalContainer">
                <h4 className="basketTotalTitle">TOTAL</h4>
                <h4 className="basketTotal">R$ {basketProps.cartCost},00</h4>
            </div>
        </div>
        <div className="Pagamento">
            <button onClick={() => fimPedido()} className="Finalizar">Finalizar pedido</button>
        </div>       
        </div>
    )

}

const mapDispatchToProps = dispatch => bindActionCreators({ removeItem, fimPedido }, dispatch)
const mapStateToProps = state => ({
        basketProps: state.basketState
})

export default connect(mapStateToProps, mapDispatchToProps)(Cart)