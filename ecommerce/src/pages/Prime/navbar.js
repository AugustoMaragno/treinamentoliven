import React, { useEffect } from 'react';
import { Link } from 'react-router-dom'
import Logo from '../../assets/teste1.png'

import { connect } from 'react-redux'
import { getNumbers } from '../../actions/getAction'

const Navbar = (props) => {

    useEffect(() => {
        getNumbers()
    }, [])

    return (
        <nav className="nav-wrapper">
            <div className="container">
                <Link to="/app"><img className="Logo" src={Logo} alt="Teste Logo" /></Link>
                <ul className="right">
                    <li><Link to="/app">Loja</Link></li>
                    <li><Link to="/cart"><i className="material-icons">shopping_cart</i></Link></li>
                    <span>{props.basketProps.basketNumbers}</span>
                </ul>
            </div>
        </nav>
    )
}

const mapStateToProps = state => ({
    basketProps: state.basketState
})

export default connect(mapStateToProps, { getNumbers })(Navbar);