import React, { Component} from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import { Provider } from 'react-redux'
import Store from '../../store'

import Navbar from './navbar'
import Menu from '../Menu/menu'
import Cart from '../Cart/cart'

export default class Prime extends Component {

    render() {
        return (
            <Provider store={Store}>
                <BrowserRouter>
                    <div className="Prime">
                        <Navbar />
                        <Switch>
                            <Route exact path="/app" component={Menu} />
                            <Route path="/cart" component={Cart} />
                        </Switch>
                    </div>
                </BrowserRouter>
            </Provider>
        )
    }
}