import React, { Component } from "react"
import {Link, withRouter} from "react-router-dom" 

import Logo from "../../assets/teste.png"
//import api from "../../services/api"
//import {login } from "../../services/auth"

import {Form, Container} from "./styles"

class SignIn extends Component {
    state = {
        email: "",
        senha: "",
        error: ""
    }

    handleSignIn = async e => {
        e.preventDefault();
        const {email, senha} = this.state
        if (!email || !senha){
            this.setState({ error: "Preencha e-mail e senha para continuar!"})
        } else {
            try {
                //const response = await api.post("/sessions", {email, senha})
               // login(response.data.token);
                if (email === "gu@to" && senha === "123"){
                    this.props.history.push("/app")
                } else {
                    throw "myException"
                }

            } catch (err) {
                this.setState({
                    error: "Dados informados estão incorretos."
                })
            }
        }
    }

    render() {
        return (
            <Container>
                <Form onSubmit={this.handleSignIn}>
                    <img src={Logo} alt="Teste Logo" />
                    {this.state.error && <p> {this.state.error}</p>}
                    <input
                        type="email"
                        placeholder="Endereço de email"
                        onChange={e => this.setState({ email: e.target.value })}
                    />
                    <input
                        type="password"
                        placeholder="Senha"
                        onChange={e => this.setState({ senha: e.target.value})}
                    />
                    <button type="submit">Entrar</button>
                    <hr />
                    <Link to="/signup">Criar uma nova conta</Link>
                </Form>
            </Container>
        )
    }
}

export default withRouter(SignIn);