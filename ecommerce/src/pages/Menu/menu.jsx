import './menu.css'
import api from '../../components/api'
import React, { Component } from 'react'
import { getInfos } from '../../actions/getData'
import Card from '../Cards/cards'
import { connect } from 'react-redux'


class Menu extends Component {
    constructor(props) {
        super(props)
        this.state = { produtos: [] }
    }

    async componentDidMount() {
        const response = await api.get('')
        this.setState({ produtos: response.data })
        this.props.getInfos(response.data)
    }

    render() {
        const { produtos } = this.state

        return (
            <div className="Menu">
                <div className="Cards">
                    {produtos.map(produto => (
                        <Card key={produto.id} produtos={produto} />
                    ))}
                </div>
            </div>
        )
    }
}

export default connect(null, { getInfos })(Menu) 