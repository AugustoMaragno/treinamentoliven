import React, { Component } from "react"
import { Link, withRouter } from "react-router-dom"
// import api from "../../services/api"
import Logo from "../../assets/teste.png"

import { Form, Container } from "./styles"

class SignUp extends Component {
    state = {
        nome: "",
        email: "",
        senha: "",
        data: "",
        cpf: "",
        error: ""
    }

    handleSignUp = async e => {
        e.preventDefault();

        const { nome, email, senha, data, cpf } = this.state;

        if (!nome || !email || !senha || !data || !cpf) {
            this.setState({ error: "Preencha todos os dados para se cadastrar" })
        } else {
            try {
               //await api.post("/users", { nome, email, senha, data, cpf })
                this.props.history.push("/");
            } catch (err) {
                console.log(err)
                this.setState({ error: "Ocorreu um erro ao registrar sua conta." })
            }
        }
    }



    render() {
        return (
            <Container>
                <Form onSubmit={this.handleSignUp}>
                    <img src={Logo} alt="Teste Logo" />
                    {this.state.error && <p>{this.state.error}</p>}
                    <input
                        type="text"
                        placeholder="Nome de usuário"
                        onChange={e => this.setState({ nome: e.target.value })} />

                    <input
                        type="email"
                        placeholder="E-mail"
                        onChange={e => this.setState({ email: e.target.value })} />

                    <input
                        type="password"
                        placeholder="Senha"
                        autoComplete="new-password"
                        onChange={e => this.setState({ senha: e.target.value })} />

                    <input
                        type="date"
                        placeholder="Data de Nascimento"
                        onChange={e => this.setState({ data: e.target.value })} />

                    <input
                        type="cpf"
                        placeholder="CPF"
                        maxLength='14'
                        onChange={e => this.setState({ cpf: e.target.value })} />

                    <button type="submit">Cadastrar</button>

                    <hr />

                    <Link to="/"> Fazer login</Link>
                </Form>
            </Container>
        )
    }
}

export default withRouter(SignUp);