import { REMOVE_ITEM } from './types'

export const removeItem = (produtos) => {
   
    return{
        type: REMOVE_ITEM,
        payload: produtos
}
}
