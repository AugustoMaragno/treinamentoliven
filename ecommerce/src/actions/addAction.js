import { ADD_PRODUCT_BASKET } from './types'

export const addBasket = (produtos) => {
   
    return{
        type: ADD_PRODUCT_BASKET,
        payload: produtos
}
}
