import { FIM_PEDIDO} from './types'

export const fimPedido = (produto) => {
   
    return{
        type: FIM_PEDIDO,
        payload: produto
}
}
