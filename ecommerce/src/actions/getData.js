import { GET_INFOS } from './types'

export const getInfos = (produtos) => {
    return{
        type: GET_INFOS,
        payload: produtos
}
}
