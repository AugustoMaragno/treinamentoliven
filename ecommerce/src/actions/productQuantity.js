import { INCREASE_QUANTITY, DECREASE_QUANTITY } from './types'

export const productQuantity = (action, produtos) => {
    return (dispatch) =>  {  
    dispatch({
        type: action === "increase" ? INCREASE_QUANTITY : DECREASE_QUANTITY,
        payload: produtos
    })
}
}