import { ADD_PRODUCT_BASKET, GET_NUMBERS_BASKET, GET_INFOS, INCREASE_QUANTITY, DECREASE_QUANTITY, REMOVE_ITEM, FIM_PEDIDO } from "../actions/types"
import { element } from "prop-types"


const initialState = {
    basketNumbers: 0,
    cartCost: 0,
    produtos: [],
    prodAlod: [],

}

export default (state = initialState, action) => {
    let selec = ''
    switch (action.type) {
        case GET_INFOS:
            return { ...state, produtos: action.payload }
        case ADD_PRODUCT_BASKET:
            action.payload.count = 1
            state.prodAlod.push(action.payload)
            console.log(state.prodAlod)
            return {
                ...state,
                basketNumbers: state.basketNumbers + 1,
                cartCost: state.cartCost + parseFloat(action.payload.price),

            }
        case GET_NUMBERS_BASKET:
            return {
                ...state
            }
        case REMOVE_ITEM:

            selec = state.prodAlod.find(element => element = action.payload)
            state.prodAlod.splice(selec, 1)

            return {
                ...state,
                basketNumbers: state.basketNumbers - 1,
                cartCost: state.cartCost - parseFloat(action.payload.price),

            }
        case INCREASE_QUANTITY:
            return {
                ...state
            }

        case FIM_PEDIDO:
            state.prodAlod = []
            state.cartCost = 0
            state.basketNumbers = 0
            console.log(state.prodAlod)
            return {
                ...state
            }
        case DECREASE_QUANTITY:
            return {
                ...state
            }
        default:
            return state
    }
}