import React, { Fragment } from 'react';
import "./styles/global"
import './App.css';
import Routes from './routes'
import GlobalStyle from './styles/global'
import { BrowserRouter } from 'react-router-dom';

const App = () => (
  <Fragment>
    <BrowserRouter>
      <Routes />
      <GlobalStyle />
    </BrowserRouter>
  </Fragment>

)
export default App;
